package git;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("""
                This program accepts the input of 2 numbers from the user.
                It then divides the first number by the second entered number.
                Do not enter a 0 for the second number, as we all know you cannot
                divide by zero, so if you put a zero for the second number, you will
                be in BIG TROUBLE. 
                                
                """);

        float num1 = getFloat(input);

        float num2 = getFloat2(input);

        if (num2 == 0){
            System.out.print("Come on now, i thought we talked about this. Try again.");
            num2 = getFloat2(input);
        }

        if (num2 == 0){
            System.out.print("YOU HAD YOUR CHANCE. NOW GET OUTTA HERE AND TRY AGAIN LATER.");
            System.exit(1);
        }

        System.out.print("\nBoom. Look at that. Definitely easier than just using a calculator. Right?\n" +
                num1 + " divided by " + num2 + " is equal to " + doDivision(num1, num2));


    }


    private static float getFloat(Scanner input) {
        System.out.print("Enter the first number: ");
        float validFloat = -1;
        while (input.hasNext()) {
            if (input.hasNextFloat()) {
                validFloat = input.nextFloat();
                break;
            } else {
                System.out.println("Error: Input must be a number.");
                System.out.print("Enter the first number: ");
                input.next();
            }
        }
        return validFloat;
    }

    private static float getFloat2(Scanner input) {
        System.out.print("Enter the second number. Better not be a zero: ");
        float validFloat = -1;
        while (input.hasNext()) {
            if (input.hasNextFloat()) {
                validFloat = input.nextFloat();
                break;
            } else {
                System.out.println("Error: Input must be a number.");
                System.out.print("Enter the second number. Better not be a zero: ");
                input.next();
            }
        }

        return validFloat;
    }


    private static float doDivision(float num1, float num2){
        return num1/num2;
    }
}


